#!/usr/bin/env bash

cd actors

if [ ! -d node_modules ]; then
	npm i
fi

cd ../static

if [ ! -d node_modules ]; then
	npm i
fi

echo "Testing..."

cd ../actors

npm run test
if [ $? -ne 0 ]; then
   echo "TESTS FAILED"
   exit 1
fi

cd ../static

npm run test
if [ $? -ne 0 ]; then
   echo "TESTS FAILED"
   exit 1
fi
