import '@babel/polyfill'

import './index.less'
import './index.scss'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './app/App'

ReactDOM.render(
  <App/>,
  document.getElementById('root')
)
