import _ from 'lodash'

const misc = {
  calls: {}, // calls to manage when actives

  messageType: {
    ERROR: {color: 'red', icon: ':('},
    INFO: {color: 'grey', icon: '!'},
    SUCCESS: {color: 'green', icon: ':)'},
    WARNING: {color: 'yellow', icon: ':|'}
  },

  refreshing: false,

  resetValue: '_nil_',

  camelToSnake(s) {
    return s.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase()
  },

  capitalize(a) {
    return a.toLowerCase().split(' ').map(s => s.charAt(0).toUpperCase() + s.substring(1)).join(' ')
  },

  checkErrors(callResponse) {
    return callResponse.err || callResponse.res.body.error
  },

  checkMaxTimeIntervalRespected(minDate, maxDate) {
    // 1 week, 7 days
    return minDate >= new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate() - 7, maxDate.getHours(), maxDate.getMinutes(), maxDate.getSeconds())
  },

  checkRange(id) {
    const el = document.getElementById(id)
    let v = parseInt(el.value, 10)
    const elAttrMin = el.getAttribute('min')
    const elAttrMax = el.getAttribute('max')
    let min = null
    let max = null

    if (elAttrMin) {
      min = parseInt(elAttrMin, 10)
    }
    if (elAttrMax) {
      max = parseInt(elAttrMax, 10)
    }
    if (this.empty(el.value)) {
      v = 0
    }
    if (min !== null && v < min) {
      v = min
    }
    if (max !== null && v > max) {
      v = max
    }
    document.getElementById(id).value = v

    return v
  },

  createFunction(name, params, body) {
    return ((name, body) => ({[name](...args) {return body(...args)}}[name]))(name, new Function(params, body))
  },

  dotNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  },

  empty(a) {
    return a === misc.resetValue || a === null || a === 'null' || typeof a === 'undefined' || a === '' || a === 0 || a === -1 || !a || JSON.stringify(a) === '{}' || (typeof a.length !== 'undefined' && a.length === 0)
  },

  // formatDate(date) {
  //   const year = date.getFullYear()
  //   const month = ('0' + (date.getMonth() + 1)).substr(-2)
  //   const day = ('0' + date.getDate()).substr(-2)
  //   const hours = ('0' + date.getHours()).substr(-2)
  //   const minutes = ('0' + date.getMinutes()).substr(-2)
  //   const seconds = ('0' + date.getSeconds()).substr(-2)

  //   return year + '-' + month + '-' + day + 'T' + hours + ':' + minutes + ':' + seconds + 'Z'
  // },

  hash() {
    let text = ''
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

    new Array(5).fill(undefined).map(() => {
      text += possible.charAt(Math.floor(Math.random() * possible.length))
    })

    return text
  },

  hasLocalProperty(category, property) {
    const c = localStorage.getItem(category)

    if (this.empty(c)) {
      return false
    }

    return c.indexOf(property) > -1
  },

  messageTypeByCode(code) {
    let mt = this.messageType.INFO

    if (typeof code === 'number') {
      mt = this.messageType[code < 400 ? 'SUCCESS' : 'ERROR']
    }

    return mt
  },

  // now() {
  //   return this.formatDate(new Date())
  // },

  objectLevelCompare(a, b) {
    for (const k in a) {
      if (a[k] !== b[k]) {
        return false
      }
    }

    return true
  },

  snakeToCamel(s) {
    return s.replace(/(\-\w)/g, m => m[1].toUpperCase())
  },

  sortByName(arr) {
    return arr.sort((a, b) => {
      if (a.name.toLowerCase() < b.name.toLowerCase()) {
        return -1
      } else if (a.name.toLowerCase() > b.name.toLowerCase()) {
        return 1
      }

      return 0
    })
  },

  undef(a) {
    return typeof a === 'undefined'
  }
}

export default misc
