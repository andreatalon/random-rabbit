import misc from './misc'

const config = {
  actionTimeout: 3000,

  headers() {
    return {
      'Content-Type': 'application/json'
    }
  },

  service(which, uri) {
    return this.url()[which] + uri
  },

  url() {
    switch (window.location.port) {
    case '8765':
      return {
        // be: 'http://localhost:8765'
        ws: 'ws://localhost:8766'
      }
    default:
      return {
        // be: '',
        ws: ''
      }
    }
  }
}

export default config
