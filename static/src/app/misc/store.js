import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import {connectRouter, routerMiddleware} from 'connected-react-router'
import {combineEpics, createEpicMiddleware} from 'redux-observable'
import * as epics from '../actions/epics'
import history from './history'
import numbers from '../reducers/numbers'
import workplace from '../reducers/workplace'

const epicMiddleware = createEpicMiddleware()

const store = createStore(
  combineReducers({
    numbers,
    workplace,
    router: connectRouter(history)
  }),
  undefined, // initialState,
  compose(
    applyMiddleware(
      routerMiddleware(history),
      epicMiddleware
      // createEpicMiddleware(combineEpics(...Object.values(epics)))
      // ... other middlewares ...
    ),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)

epicMiddleware.run(combineEpics(...Object.values(epics)))

export default store
