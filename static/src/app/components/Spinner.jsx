import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

class Spinner extends Component {
  render() {
    return this.props.activeCalls.length > 0 ? <img src="/assets/images/loader" className="spin-me"/> : null
  }
}

Spinner.propTypes = {
  activeCalls: PropTypes.array.isRequired
}

function mapStateToProps(state) {
  return {
    activeCalls: state.workplace.activeCalls
  }
}

export default connect(
  mapStateToProps
)(Spinner)
