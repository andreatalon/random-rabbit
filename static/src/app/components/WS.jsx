import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {actions} from '../actions/actions'
import _ from 'lodash'
import misc from '../misc/misc'
import config from '../misc/config'

class WS extends Component {
  componentDidMount() {
    let numbers = localStorage.getItem('numbers')

    if (!misc.empty(numbers)) {
      try {
        numbers = JSON.parse(numbers)

        this.props.storeNumbers(numbers)
      } catch (e) {
        console.log(e)
      }
    }

    this.state = this.initState()
  }

  initState() {
    const connection = new WebSocket(config.url().ws)

    let heartbeat = null

    connection.onopen = () => {
      this.props.flashMessageAndReset('Connected! Receiving notifications...', misc.messageType.SUCCESS)

      heartbeat = setInterval(() => connection.send('keep me alive'), 5000)
    }

    connection.onerror = err => {
      this.props.flashMessageAndReset('Connection error!', misc.messageType.ERROR)
    }

    connection.onmessage = event => {
      const data = JSON.parse(event.data)

      switch (data.type) {
      case 'number':
        this.props.storeNumber(data.value)

        break
      default:
      }
    }

    return {
      heartbeat,
      connection
    }
  }

  disconnect() {
    clearInterval(this.state.heartBeat)
    this.state.connection.close()
  }

  render() {
    return null
  }
}

function mapDispatchToProps(dispatch) {
  return {
    storeNumber: bindActionCreators(actions.storeNumber, dispatch),
    storeNumbers: bindActionCreators(actions.storeNumbers, dispatch),
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch)
  }
}

export default connect(
  null,
  mapDispatchToProps
)(WS)
