import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Route} from 'react-router-dom'
import {actions} from '../actions/actions'
import FlashMessage from './FlashMessage'
import WS from './WS'
import _ from 'lodash'
import misc from '../misc/misc'

class Workplace extends Component {
  manageResponse() {
    if (misc.empty(this.props.callResponse)) {
      return
    }

    let r = this.props.callResponse

    if (!misc.empty(r)) {
      if (r.response) {
        r = r.response
      }

      const rc = typeof r.code === 'number' ? `${r.code}: ` : ''
      const rm = misc.undef(r.message) ? '' : r.message
      let message = ''

      if (typeof rm === 'string') {
        message = rm
      } else if (typeof rm === 'object') {
        message = JSON.stringify(rm).replace(/[\{\}\(\)\[\]\"]/g, '')
      }

      this.props.flashMessageAndReset(`${rc} ${message}`, misc.messageTypeByCode(r.code))
    }
  }

  render() {
    const parentProps = {
      ...this.props
    }

    this.manageResponse()

    return (
      <Route render={props => (
        <div className='workplace'>
          <FlashMessage/>
          <WS/>
          <div className='body'>
            <this.props.component {...props} parentProps={parentProps}/>
          </div>
        </div>
      )}/>
    )
  }
}

Workplace.propTypes = {
  component: PropTypes.func,
  waiting: PropTypes.func,
  refresh: PropTypes.func,
  message: PropTypes.string,
  flashMessage: PropTypes.func
}

function mapStateToProps(state) {
  return {
    message: state.workplace.message,
    callResponse: state.workplace.callResponse
  }
}

function mapDispatchToProps(dispatch) {
  return {
    waiting: bindActionCreators(actions.waiting, dispatch),
    refresh: bindActionCreators(actions.refresh, dispatch),
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Workplace)
