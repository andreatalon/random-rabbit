import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts'

class Home extends Component {
  render() {
    const data = this.props.numbers.map(n => ({value: n}))

    return (
      <div style={{textAlign: 'center'}} width={'100%'}>
        <div style={{display: 'inline-block', width:'100%'}}>
          <h1>RANDOM RABBIT</h1>
        </div>
        <div style={{display: 'inline-block', width:'324px', height:'240px'}}>
          <pre>
            <code>{`
                              __
                    /\\    .-" /
                    /  ; .'  .'
                  :   :/  .'
                    \\  ;-.'
      .--""""--..__/     '.
    .'           .'    'o  \\
    /                    '   ;
  :                  \\      :
.-;        -.         '.__.-'
:  ;          \\     ,   ;
'._:           ;   :   (
    \\/  .__    ;    \\   '-.
    ;     "-,/_..--"'-..__)
    '""--.._:
            `}</code>
          </pre>
        </div>
        <br/>
        <br/>
        <div style={{display: 'inline-block', width:'100%'}}>
          <strong>LAST</strong>: <span style={{fontSize: '1.3em'}}>{this.props.numbers[this.props.numbers.length - 1]}</span>
          <br/>
          <strong>ALL</strong>: [..., {this.props.numbers.join(', ')}]
        </div>
        <br/>
        <br/>
        <div style={{display: 'inline-block', width:'324px', height:'240px'}}>
          <ResponsiveContainer width={324} height={240}>
            <LineChart data={data}>
              <Line type='monotone' dataKey='value' stroke='#8884d8' strokeWidth={2}/>
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
    )
  }
}

Home.propTypes = {
  numbers: PropTypes.array
}

function mapStateToProps(state) {
  return {
    numbers: state.numbers.numbers
  }
}

export default connect(
  mapStateToProps,
  null
)(Home)
