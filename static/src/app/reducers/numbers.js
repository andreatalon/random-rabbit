import {types} from '../actions/actions'
import _ from 'lodash'
import misc from '../misc/misc'

const initialState = {
  howmany: 10,
  numbers: []
}

function numbers(state = initialState, action) {
  switch (action.type) {
  case types.STORE_NUMBERS: {
    let numbers = action.numbers

    if (numbers.length > initialState.howmany) {
      numbers = numbers.slice(numbers.length - initialState.howmany, numbers.length)
    }

    localStorage.setItem('numbers', JSON.stringify(numbers))

    return Object.assign({}, state, {
      numbers
    })
  }

  case types.STORE_NUMBER: {
    const numbers = state.numbers.slice()

    numbers.push(action.number)

    if (numbers.length > initialState.howmany) {
      numbers.shift()
    }

    localStorage.setItem('numbers', JSON.stringify(numbers))

    return Object.assign({}, state, {
      numbers
    })
  }

  default:
    return state
  }
}

export default numbers
