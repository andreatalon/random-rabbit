import {types} from '../actions/actions'
import _ from 'lodash'
import misc from '../misc/misc'

const initialState = {
  activeCalls: [],
  blockingCalls: [],
  callResponse: null,
  flashedMessage: null,
  silentCalls: []
}

function workplace(state = initialState, action) {
  switch (action.type) {
  case types.FLASH_MESSAGE:
    return Object.assign({}, state, {
      flashedMessage: action.flashedMessage,
      messageType: action.messageType
    })

  case types.MANAGE_RESPONSE:
    return Object.assign({}, state, {
      callResponse: action.callResponse
    })

  case types.WAITING: {
    const sacs = state.activeCalls
    const sbcs = state.blockingCalls
    const sscs = state.silentCalls

    const aacs = misc.empty(action.activeCalls) ? [] : action.activeCalls
    const abcs = misc.empty(action.blockingCalls) ? [] : action.blockingCalls
    const ascs = misc.empty(action.silentCalls) ? [] : action.silentCalls

    const activeCalls = action.toggle ? _.uniq(_.concat(sacs, aacs)) : _.difference(sacs, aacs)
    const blockingCalls = action.toggle ? _.uniq(_.concat(sbcs, abcs)) : _.difference(sbcs, abcs)
    const silentCalls = action.toggle ? _.uniq(_.concat(sscs, ascs)) : _.difference(sscs, ascs)

    return Object.assign({}, state, {
      activeCalls,
      blockingCalls,
      silentCalls
    })
  }

  default:
    return state
  }
}

export default workplace
