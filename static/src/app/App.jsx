import React from 'react'
import {Provider} from 'react-redux'
import {ConnectedRouter as Router} from 'connected-react-router'
import {Redirect, Switch} from 'react-router-dom'
import Home from './components/Home'
import NoMatch from './components/NoMatch'
import Workplace from './components/Workplace'
import store from './misc/store'
import history from './misc/history'

const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Redirect exact from='/' to='/home'/>
        <Workplace exact path='/home' component={Home}/>
        <Workplace path='*' component={NoMatch}/>
      </Switch>
    </Router>
  </Provider>
)

export default App
