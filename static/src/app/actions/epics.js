import {concat, empty, of} from 'rxjs'
import {ajax} from 'rxjs/ajax'
import {catchError, delay, filter, mergeMap, switchMap, timeout, tap} from 'rxjs/operators'
import {actions, types} from './actions'

export const flashMessageAndResetEpic = action$ =>
  action$.pipe(filter(action => action.type === types.FLASH_MESSAGE_AND_RESET), switchMap(action =>
    concat(
      of(actions.flashMessage(null)),
      of(actions.flashMessage(action.flashedMessage, action.messageType)),
      of(actions.flashMessage(null)).pipe(delay(6000))
    )))

export const manageResponseAndResetEpic = action$ =>
  action$.pipe(filter(action => action.type === types.MANAGE_RESPONSE_AND_RESET), switchMap(action =>
    concat(
      of(actions.manageResponse(action.callResponse)),
      of(actions.manageResponse(null))
    )))
