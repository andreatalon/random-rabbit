import misc from '../misc/misc'

const types = {}, actions = {}, defs = {
  waiting: ['toggle', 'activeCalls', 'blockingCalls', 'silentCalls'],
  refresh: [],
  flashMessage: ['flashedMessage', 'messageType'],
  flashMessageAndReset: ['flashedMessage', 'messageType'],
  manageResponse: ['callResponse'],
  manageResponseAndReset: ['callResponse'],

  storeNumber: ['number'],
  storeNumbers: ['numbers']
}

Object.keys(defs).map(def => {
  const params = defs[def].join(', ')
  const type = misc.camelToSnake(def)

  types[type] = String(type)
  actions[def] = misc.createFunction(def, params, `return {type:'${type}', ${params}}`)
})

export {
  types,
  actions,
  defs
}
