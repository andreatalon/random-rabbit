'use strict'

import workplace from '../../../../src/app/reducers/workplace'
import {actions, types} from '../../../../src/app/actions/actions'

describe('workplace reducer', () => {
  it('should be waiting with no blocking or silenting calls', () => {
    const initialState = {
      activeCalls: 0,
      blockingCalls: 0,
      silentCalls: 0
    }

    const state = workplace(initialState, {
      type: types.WAITING,
      waiting: true,
      blocking: null,
      silenting: false
    })

    expect(state.activeCalls.length).toEqual(0)
    expect(state.blockingCalls.length).toEqual(0)
    expect(state.silentCalls.length).toEqual(0)
  })
})
