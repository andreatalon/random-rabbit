#!/usr/bin/env bash

cd actors

if [ ! -d node_modules ]; then
	npm i
fi

cd ../static

if [ ! -d node_modules ]; then
	npm i
fi

echo "Building..."

cd ../actors

npm run build
if [ $? -ne 0 ]; then
   echo "BUILD FAILED"
   exit 1
fi

cd ../static

npm run build
if [ $? -ne 0 ]; then
   echo "BUILD FAILED"
   exit 1
fi
