# Random Rabbit

1. Clone the repository in a local folder
2. In command prompt enter these instructions:
  ```sh
  docker-compose up -d
  docker-compose exec app sh
  $ ./01-test.sh && ./02-build.sh && ./03-run.sh
  ```
3. Open the following url in a web browser: "http://localhost:8765"
4. Enjoy! :)
