import 'regenerator-runtime/runtime'

import connect from 'connect'
import historyApiFallback from 'connect-history-api-fallback'
import http from 'http'
import gzipStatic from 'connect-gzip-static'
import log4js from 'log4js'
import morgan from 'morgan'
import serveStatic from 'serve-static'
import WebSocket from 'websocket'

import {prepare} from './workspace'

const rand = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

const exec = async() => {
  const logger = prepare()

  console.log('Here I am!')
  logger.debug('Here I am!')

  const app = connect()

  app.use(morgan('composed', {
    stream: {
      write: log => {
        (log4js.getLogger()).debug(log)
      }
    }
  }))

  app.use(function(req, res, next) {
    if (req._parsedUrl.pathname.match(/healthcheck/)) {
      res.writeHead(200)
      res.end('I\'m fine!')

      return
    }
    next()
  })

  const staticApp = '../static/public'

  app.use(serveStatic(staticApp))
  app.use(historyApiFallback())
  app.use(gzipStatic(staticApp, {maxAge: 86400000, override: true}))

  app.use(function(req, res, next) {
    if (req._parsedUrl.pathname.match(/index\.(js|css)$/)) {
      req.url += '.gz'
      res.setHeader('Content-Encoding', 'gzip')
    } else {
      req.url = '/'
    }
    next()
  })

  app.listen(80)

  const server = http.createServer((req, res) => {
    // Qui possiamo processare la richiesta HTTP
    // Dal momento che ci interessano solo le WebSocket, non dobbiamo implementare nulla
  })

  server.listen(1337, () => {})

  const wsServer = new WebSocket.server({
    httpServer: server
  })

  wsServer.on('request', request => {
    const connection = request.accept(null, request.origin)

    connection.on('connection', connection => {
      logger.debug(connection)
    })

    connection.on('message', message => {
      logger.debug(message)
    })

    connection.on('close', connection => {
      logger.debug(connection)
    })
  })

  console.log('http://localhost:8765')

  let i = 0

  i = 1

  while (i) {
    const r = rand(1, 1000)

    // console.log(r)
    logger.debug(r)

    wsServer.connections.forEach(connection => {
      connection.send(JSON.stringify({type: 'number', value: r}))
    })

    await new Promise(resolve => setTimeout(resolve, rand(1000, 10000)))
  }
}

export {
  exec
}
